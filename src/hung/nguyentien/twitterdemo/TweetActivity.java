package hung.nguyentien.twitterdemo;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TweetActivity extends Activity implements OnClickListener {

	private Button btnTweet;
	private EditText txtTweet;

	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tweet);
		btnTweet = (Button) findViewById(R.id.btnTweet);
		txtTweet = (EditText) findViewById(R.id.txtTweet);
		btnTweet.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		String status = txtTweet.getText().toString();
		if (status != null && status.trim().length() > 0) {
			new TweetTask().execute(status);
		} else {
			Toast.makeText(this, R.string.please_input_message,
					Toast.LENGTH_LONG).show();
		}
	}

	public class TweetTask extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(TweetActivity.this);
			progressDialog.setMessage(getResources().getString(
					R.string.please_wait));
			progressDialog.setIndeterminate(false);
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

		protected String doInBackground(String... params) {
			String status = params[0];
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(Constants.TWITTER_CONSUMER_KEY);
				builder.setOAuthConsumerSecret(Constants.TWITTER_CONSUMER_SECRET);

				String access_token = Utils.getStringPreference(
						TweetActivity.this, Constants.KEY_OAUTH_TOKEN);
				String access_token_secret = Utils.getStringPreference(
						TweetActivity.this, Constants.KEY_OAUTH_SECRET);

				AccessToken accessToken = new AccessToken(access_token,
						access_token_secret);
				Twitter twitter = new TwitterFactory(builder.build())
						.getInstance(accessToken);

				twitter.updateStatus(status);
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String file_url) {
			progressDialog.dismiss();
			Toast.makeText(TweetActivity.this, R.string.tweet_success,
					Toast.LENGTH_LONG).show();
		}

	}
}
