package hung.nguyentien.twitterdemo;

import android.content.Context;
import android.content.SharedPreferences;

public class Utils {

	public static void saveStringPreference(Context context, String key,
			String value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static void saveFloatPreference(Context context, String key,
			float value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putFloat(key, value);
		editor.commit();
	}

	public static void saveBooleanPreference(Context context, String key,
			boolean value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public static void saveIntPreference(Context context, String key, int value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public static void saveLongPreference(Context context, String key,
			long value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putLong(key, value);
		editor.commit();
	}

	public static String getStringPreference(Context context, String key) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getString(key, "");
	}

	public static boolean getBooleanPreference(Context context, String key) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getBoolean(key, false);
	}

	public static int getIntPreference(Context context, String key) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getInt(key, 0);
	}

	public static long getLongPreference(Context context, String key) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getLong(key, 0);
	}

	public static float getFloatPreference(Context context, String key) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
		return sharedPreferences.getFloat(key, 0);
	}
}
