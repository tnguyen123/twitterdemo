package hung.nguyentien.twitterdemo;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class SignInActivity extends Activity {

	private WebView webView;
	private Twitter twitter;
	private RequestToken requestToken;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signin);

		webView = (WebView) findViewById(R.id.webView);
		setUpWebView();
		new SignInTask().execute();
	}

	private void setUpWebView() {
		webView.setVerticalScrollBarEnabled(false);
		webView.setHorizontalScrollBarEnabled(false);
		webView.setWebViewClient(new TwitterWebViewClient());
		webView.getSettings().setJavaScriptEnabled(true);
	}

	public class TwitterWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Log.d("CALLBACK_URL", url);
			if (url.startsWith(Constants.TWITTER_CALLBACK_URL)) {
				Uri uri = Uri.parse(url);
				String verifier = uri
						.getQueryParameter(Constants.URL_TWITTER_OAUTH_VERIFIER);
				new GetAcessTokenTask().execute(verifier);
				return true;
			} else if (url.startsWith("authorize")) {
				return false;
			}
			return true;
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			super.onReceivedError(view, errorCode, description, failingUrl);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
		}
	}

	public class GetAcessTokenTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... params) {
			try {
				AccessToken accessToken = twitter.getOAuthAccessToken(
						requestToken, params[0]);

				Utils.saveStringPreference(SignInActivity.this,
						Constants.KEY_OAUTH_TOKEN, accessToken.getToken());
				Utils.saveStringPreference(SignInActivity.this,
						Constants.KEY_OAUTH_SECRET,
						accessToken.getTokenSecret());
				Utils.saveBooleanPreference(SignInActivity.this,
						Constants.KEY_TWITTER_LOGIN, true);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Intent intent = new Intent(SignInActivity.this, TweetActivity.class);
			startActivity(intent);
			SignInActivity.this.finish();
		}

	}

	public class SignInTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(Constants.TWITTER_CONSUMER_KEY);
			builder.setOAuthConsumerSecret(Constants.TWITTER_CONSUMER_SECRET);
			Configuration configuration = builder.build();

			TwitterFactory factory = new TwitterFactory(configuration);
			twitter = factory.getInstance();

			try {
				requestToken = twitter
						.getOAuthRequestToken(Constants.TWITTER_CALLBACK_URL);

			} catch (TwitterException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			webView.loadUrl(requestToken.getAuthenticationURL());
		}
	}
}
